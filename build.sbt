import sbt.Keys._
import AssemblyKeys._

assemblySettings

name := "graph"

version := "1.0"

scalaVersion := "2.10.1"

libraryDependencies ++= Seq(
 "org.scalastyle" %% "scalastyle" % "0.3.2",
 "org.scalatest" %% "scalatest" % "1.9.1" % "test",
 "junit" % "junit" % "4.10" % "test"
)

scalacOptions ++= Seq("-unchecked", "-deprecation")
