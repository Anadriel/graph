package me.anadriel

import scala.collection.immutable.Queue

trait Graph[W, T] {

  var value: T = null.asInstanceOf[T]
  var inEdges: List[Edge[W, T]] = Nil
  var outEdges: List[Edge[W, T]] = Nil
  
  def graphsByDepth: Set[Graph[W, T]] = {
    def recFun(graphs: Set[Graph[W, T]], graph: Graph[W, T]): Set[Graph[W, T]] =
      if (!graphs(graph)) {
        val part = graph.outEdges.map(_.goal).foldLeft(graphs + graph)((grs, gr) => recFun(grs, gr))
        graph.inEdges.map(_.base).foldLeft(part)((grs, gr) => recFun(grs, gr))
      } else graphs

    recFun(Set(), this)
  }

  def graphsByBreadth: Set[Graph[W, T]] = {
    def recFun(graphs: Set[Graph[W, T]], queue: Queue[Graph[W, T]]): Set[Graph[W, T]] =
      if (queue.nonEmpty && !graphs(queue.head)) {
        val part = queue.head.outEdges.map(_.goal).foldLeft(queue.tail)((acc, gg) => acc :+ gg)
        recFun( graphs + queue.head, queue.head.inEdges.map(_.base).foldLeft(part)((acc, gg) => acc :+ gg))
      } else graphs

    recFun(Set(), Queue(this))
  }

  def connect(parent: T, withWeight: W, child: T) {
    val parentGraph: Graph[W, T] =
      if (value == null) {
        value = parent
        this
      }
      else
        findNode(parent).getOrElse(Graph.one(parent))

    val childGraph: Graph[W, T] = findNode(child).getOrElse(Graph.one(child))

    parentGraph.outEdges = new Edge(parentGraph, withWeight, childGraph) :: parentGraph.outEdges
    childGraph.inEdges = new Edge(parentGraph, withWeight, childGraph) :: childGraph.inEdges
  }

  def findNode(n: T): Option[Graph[W, T]] = graphsByDepth.find(_.value == n)

}
case class Edge[W, T](base: Graph[W, T], weight: W, goal: Graph[W, T])

class GraphImplementation[W, T](node: T) extends Graph[W, T]{
  
  value = node
  
  override def equals(o: Any): Boolean = o match {
    case g: Graph[_, _] => g.value == value
    case _ => false
  }

  override def toString: String = "Graph(" + value + ")"
}

class WeightedGraph[T](node: T) extends GraphImplementation[Double, T](node: T) {

   def findShortestPath(from: T, to: T): List[T] = {
    var distances = graphsByDepth.map(_.value).map((_, Double.PositiveInfinity)).toMap + (from -> 0.0)
    var shortestPath: Map[T, T] = Map()

    while (distances.nonEmpty) {
      val (node, weight) = distances.minBy(_._2)
      distances = distances - node

      findNode(node).get.outEdges.filter(x => distances.map(_._1).toList.contains(x.goal.value)).foreach { outEdges =>
        val outNode = outEdges.goal.value
        if (distances(outNode) > weight + outEdges.weight) {
          distances = distances + (outNode -> (weight + outEdges.weight))
          shortestPath = shortestPath + (outNode -> node)
        }
      }
    }

    var step = to
    var path = List(step)
    while (shortestPath.contains(step)) {
      step = shortestPath(step)
      path = step :: path
    }
    path
  }
}

object Graph {
  def apply[W, T](connections: (T, W, T)*): Graph[W, T] = {
    val graph: Graph[W, T] = Graph.empty
    for (con <- connections)
      graph.connect(con._1, con._2, con._3)
    graph
  }

  def one[W, T](node: T): Graph[W, T] = new GraphImplementation[W, T] (node)
   
  def empty[W, T]: Graph[W, T] = new GraphImplementation[W, T] (null.asInstanceOf[T])
}

object WeightedGraph {
  def apply[T](connections: (T, Double, T)*): WeightedGraph[T] = {
    val graph: WeightedGraph[T] = new WeightedGraph[T](null.asInstanceOf[T])
    for (con <- connections)
      graph.connect(con._1, con._2, con._3)
    graph
  }
}

