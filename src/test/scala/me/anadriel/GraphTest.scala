package me.anadriel

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class GraphTest extends FunSuite {

  val graph: Graph[Int, String] = Graph(
    ("A", 20, "B"), ("B", 30, "C"),
    ("C", 40, "D"), ("D", 50, "A"),
    ("A", 100, "C"), ("D", 200, "B")
  )

  test("nodes by depth") {
    assert(graph.graphsByDepth.map(_.value) === Set("A", "B", "C", "D"))
  }


  test("nodes by breadth") {
    assert(graph.graphsByBreadth.map(_.value) === Set("A", "B", "C", "D"))
  }

  val weightedGraph: WeightedGraph[String] = WeightedGraph(
    ("A", 20, "B"), ("B", 30, "C"),
    ("C", 40, "D"), ("D", 50, "A"),
    ("A", 100, "C"), ("D", 200, "B")
  )

  var a = weightedGraph.graphsByDepth.map(_.value).map((_, Double.PositiveInfinity)).toMap + ("A" -> 0.0)
  val (v, _) = a.minBy(_._2)
  a = a - v
  val vv = weightedGraph.findNode(v).get

  test("shortest way") {
    assert(weightedGraph.findShortestPath("A", "D") === List("A", "B", "C", "D"))
  }

}
